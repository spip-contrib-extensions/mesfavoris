<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/mesfavoris?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add' => 'Hinzufügen',
	'add_to' => 'Meiner Liste hinzufügen',
	'ajoute_le' => 'Zugefügt am', # MODIF

	// C
	'configurer' => 'Konfigurieren',

	// I
	'info_nombre_favoris_objet' => '@nb@ Mal als Lesezeichen gespeichert', # Pas utile dans le plugin lui-même mais peut l'être dans les squelettes

	// L
	'legend_formulaire_public' => 'Veröffentlichtes Formular',
	'legend_style_formulaire' => 'Stil in der Form',
	'login_first' => 'Für diese Aktion müssen Sie angemeldet sein. Loggen Sie sich zuerst ein',

	// M
	'mes_favoris' => 'Meine Lieblingsaufnahmen',

	// R
	'remove' => 'Löschen',
	'remove_from' => 'Aus meiner Liste löschen',

	// S
	'ses_favoris' => 'Seine Lesezeichen',

	// V
	'vos_favoris' => 'Ihre Lesezeichen' # Utile pour modifier le titre de l'inclusion
);
