<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/mesfavoris?lang_cible=eu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'add' => 'Gehitu',
	'add_to' => 'Nire hautaketan gehitu',
	'ajoute_le' => 'Gehitua data honetan',

	// C
	'ce_favori' => 'Gustukoena',
	'change_to' => 'Aldatu',
	'configurer' => 'Konfiguratu',

	// D
	'dans_categorie' => 'Kategoria honetan',

	// E
	'explication_taille_formulaire' => 'Formularioaren tamaina pertsonalizatu dezakezu, sinpleki, CSSaren bidez:',

	// I
	'info_1_favori' => '1 gustukoena',
	'info_aucune_categorie' => 'Ez da kategoriak definitua izan',
	'info_categorie_filtre' => 'Irazi',
	'info_categories_tous' => 'Kategoria guztiak',
	'info_nb_favoris' => '@nb@ gustukoena',
	'info_nombre_favoris_objet' => 'Gehitua @nb@ aldiz gustukoenetan', # Pas utile dans le plugin lui-même mais peut l'être dans les squelettes

	// L
	'label_style_formulaire_bookmark' => 'Orri marka',
	'label_style_formulaire_coeur' => 'Bihotza',
	'legend_formulaire_public' => 'Inprimaki publikoa',
	'legend_style_formulaire' => 'Inprimakiaren estiloa',
	'login_first' => 'Désolé l’action requiert d’être enregistré, authentifiez-vous d’abord
Barkatu, honen egiteko erregistratua izan behar duzu, zure burua identifikatu ezazu lehenik',

	// M
	'mes_favoris' => 'Nire gustukoenak',
	'modifie_le' => 'Noiz aldatua',
	'mon_favori' => 'Nire gustukoena',

	// R
	'remove' => 'Kendu',
	'remove_from' => 'Nire hautaketatik kendu',

	// S
	'ses_favoris' => 'Haren gustukoenak',
	'son_favori' => 'Haren gustukoena',

	// V
	'vos_favoris' => 'Zure gustukoenak', # Utile pour modifier le titre de l'inclusion
	'votre_favori' => 'Zure gustukoena'
);
