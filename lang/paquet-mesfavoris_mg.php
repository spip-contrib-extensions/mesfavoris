<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-mesfavoris?lang_cible=mg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mesfavoris_description' => '(Dé)Sélectionne un objet pour gérer des objets (articles, rubriques ...) favoris pour un visiteur authentifié',
	'mesfavoris_slogan' => 'Sélection de favoris'
);
