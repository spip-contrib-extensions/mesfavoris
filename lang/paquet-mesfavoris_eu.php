<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-mesfavoris?lang_cible=eu
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mesfavoris_description' => '(Des)hautatu objektu bat kudeatzeko bisitari autentifikatu baten objektu gustukoenak (artikulu, atal...)',
	'mesfavoris_slogan' => 'Gustukoen hautaketa'
);
