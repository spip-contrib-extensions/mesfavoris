<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-mesfavoris?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mesfavoris_description' => '(De)Seleziona un oggetto per gestire oggetti (articoli, argomenti ...) preferiti per un visitatore autenticato',
	'mesfavoris_slogan' => 'Selezione dei preferiti'
);
